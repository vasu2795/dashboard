//Require mongoose package
const Mongoose = require('mongoose').Mongoose;
const mongoose = new Mongoose();
const config = require('../config/config.js')

mongoose.connect(config.development.mongo.uri)

// Database model
const UserSchema = mongoose.Schema(
    {
        username: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true,
        },
        role: {
            type: String,
            required: true,
        }
    },
    {
        collection: 'users'
    }
);

const User = mongoose.model('users', UserSchema);
console.log("here .... ");


module.exports = {
    User: mongoose.model('Users', UserSchema)
};
