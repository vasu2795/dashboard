const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const DefinePlugin = require('webpack').DefinePlugin;
const webpack =  require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const plugins = [
    // new webpack.optimize.ModuleConcatenationPlugin(),

    new ExtractTextPlugin("css/app.css"),
    new CopyWebpackPlugin([{ from: "./client/assets" }]),
    new UglifyJSPlugin({sourceMap: true})
];

plugins.push(new DefinePlugin({
        'process.env': {
            // This has effect on the react lib size
            'NODE_ENV': JSON.stringify('development')
        }
    }));


module.exports = {
  devtool: "source-map",
  entry: [__dirname + "/client/js/app.js", __dirname + "/client/css/app.css"],
  output: {
    path: __dirname + "/bundle/js",
    filename: "app.js"
  },
  resolve: {
    modules: [ "node_modules", __dirname + "/client/js" ]
  },
  devServer: {
     hot: true
   },
  module: {
    rules: [
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader?stage=0"
      },
      {
        test: /\.css$/,
        loader: "css-loader"
      },
      {
        test: /\.png$/,
        loader: "url-loader"
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: "file"
      }
    ]
  },
  plugins
};
