'use strict';

const path = require("path");
const config = {

  "development": {
      port: 8080,
      mongo: {
        // uri: 'localhost:27017/locations'
        uri: process.env.MONGO_URL || 'mongodb://userRapido:g39dVxXfRXMJwu_g@128.199.181.150:27017,139.59.123.122:27017,139.59.71.134:27017/rapidoProd'//'mongodb://rapido-dev:KRbDM=c7Jdb5nmF4@139.59.39.15:27017/rapidoDevV2'//'mongodb://localhost:27017/rapidoDevV2'
      },
      loyaltyMongo: {
        uri: process.env.LOYALTY_MONGO_URL || 'mongodb://139.59.37.220:27017/loyaltyDev'
      },
      messageBird: {
        url: 'https://rest.messagebird.com/messages',
        accessKey: 'rYeWgFrlVyjhYEwpRWVRkyajW'
      },
      redis: {
        host: 'localhost',
        port: 6379,
        prefix: 'rapido-dash'
      }
  },

  "production": {
    mongo: {
      uri:  process.env.MONGO_URL || 'mongodb://userRapido:g39dVxXfRXMJwu_g@128.199.181.150:27017,139.59.123.122:27017,139.59.71.134:27017/rapidoProd',
      options: {
        "replSet": {
          "rs_name": "rapidoReplSet",
          "readPreference": "ReadPreference.SECONDARY_PREFERRED",
          "read_preference": "ReadPreference.SECONDARY_PREFERRED",
          "slaveOk": true
        }
      }
    },
    loyaltyMongo: {
      uri: process.env.LOYALTY_MONGO_URL || 'mongodb://139.59.37.220:27017/loyaltyDev'
    },
    messageBird: {
      url: 'https://rest.messagebird.com/messages',
      accessKey: 'rYeWgFrlVyjhYEwpRWVRkyajW'
    },
    redis: {
        host: 'localhost',
        port: 6379,
        prefix: 'rapido-dash'
    }

  },

  "logs": {
    "info": {
      "name": 'info-log',
      "filename": '/var/log/loyalty/info-log.log',
      "level": 'info'
    },
    "debug": {
      "name": 'debug-log',
      "filename": '/var/log/loyalty/debug-log.log',
      "level": 'debug'
    },
    "error": {
      "name": 'error-log',
      "filename": '/var/log/loyalty/error-log.log',
      "level": 'error'
    }
  }
};

module.exports = config;
