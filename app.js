'use strict';

const
  http = require('http'),
  env = process.env.NODE_ENV || "development",
  config = require('./config/config')[env],
  express = require('express'),
  path = require('path'),
  bodyParser = require('body-parser');

const routes = require('./server/routes');

const fs = require('fs');
const morgan = require("morgan");
const json = require('morgan-json');
const format = json(':remote-addr - :remote-user :date[clf] :method :url :status :res[content-length] bytes :response-time ms :referrer :user-agent');

const initMiddlewares = (app) => {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  if ("development" === env) {
    app.use(morgan("dev"));
  } else if("production" == env) {
    var accessLogStream = fs.createWriteStream('/var/log/access.log',{flags: 'a'});
    app.use(morgan(format,{stream: accessLogStream}));
  }
}

const initRoutes = (app) => {
  app.use('/', routes);
}

const startServer = (server) => {
  server.listen(config.port, config.ip, function () {
      console.log(`Notifier server listening on ${config.port} in ${app.get('env')} mode `);
  });
}

const serverUiBundle = (app) => {
  app.use(express.static(path.join(__dirname, 'bundle')));
}


let app = express();
let server = http.createServer(app);


startServer(server);
initRoutes(app);
initMiddlewares(app);
serverUiBundle(app);

module.exports = app;
