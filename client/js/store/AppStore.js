import { createStore, combineReducers } from 'redux';
import { appStatus, merchantList, gateways } from './Application';
import navigateTo from './Navigation';

// Combine reducers and create te store
let store =  createStore(
  combineReducers({
    'Application': appStatus,
    'Navigation': navigateTo,
    'Merchants': merchantList
  }),
  {
    Application: {
      appStatus: null
    },
    Merchants: {},
    Navigation: {
      lastVisited: ''
    }
  }
);

export default store;
