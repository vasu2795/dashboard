import React from 'react';

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div className="ui visible white bottom attached sidebar menu juspay-footer">
        <div className="item juspay-navbar-logo">
          <div className="image pci-dss-logo">
            <img style={{
              width: 'auto',
              height: '40px',
              margin: '-10px'
            }} src="images/pcidss.svg" />
          </div>
        </div>

        <div className="right menu">
          <div className="item">
            <b>shoutout@rapido.bike</b>&nbsp;|&nbsp;&copy;&nbsp;Rapido&nbsp;/&nbsp;2017 - Present
          </div>
        </div>
      </div>
    );
  }
}
