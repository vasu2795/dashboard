import moment from 'moment-timezone';

/**
 * Format UTC format date with logged in user's timezone.
 * Expect "userTimestamp" in sessionStorage; If it is not there
 * default timezone to Asia/Kolkata
 *
 * @return {[type]} [description]
 */
export const formatUTCDate = (utcDate) => {
  let userTZ = sessionStorage.getItem('timezone');
  userTZ = userTZ ? userTZ : 'Asia/Kolkata';

  return moment(utcDate).tz(userTZ).format('hh:mm DD-MMM-YYYY');
};

/**
 * Format a date to fit to the standard required to version and store
 * certificate files in the database.
 *
 * Required mainly for CYBERSOURCE .p12 file versioning
 */
export const formatUTCDateISO = (utcDate) => {
  let userTZ = sessionStorage.getItem('timezone');
  userTZ = userTZ ? userTZ : 'Asia/Kolkata';

  return moment(utcDate).tz(userTZ).format('ddd MMM DD HH:MM:SS z YYYY');
};


// Given a password string, return as a percentage
// the strength of the password
export const PasswordStrength = {
  calculate: function (password) {
    if(password.length < 7) {
      return { status: this.TOO_SHORT };
    }

    let tests = [{
      regex: /[0-9]/,
      mult: 1
    }, {
      regex: /[a-z]/,
      mult: 1
    }, {
      regex: /[A-Z]/,
      mult: 1
    }, {
      regex: /[$@$!%*#?&]/,
      mult: 2
    }];

    let score = 0;
    let variety = 0;

    for(let test of tests) {
      let result = test.regex.exec(password);

      if(result) {
        variety += 1;
        score += test.mult * result.length + password.length * 1.2;
      }
    }

    let status;

    if(variety != 4) {
      status =  this.TOO_SIMPLE;
    }

    if(score > 0 && score < 40) {
      status = this.BAD;
    } else if (score >= 40 && score < 65) {
      status = this.AVERAGE;
    } else if(score >= 65 && score < 90) {
      status =  this.GOOD;
    } else if(score >= 90) {
      status =  this.STRONG;
    } else {
      status =  this.BAD;
    }

    score = Math.min(score, 100);

    return {score, status};
  },
  TOO_SIMPLE: {code: -2, label: 'Too Simple'},
  TOO_SHORT: {code: -1, label: 'Too Short'},
  BAD: {code: 0, label: 'Bad'},
  AVERAGE: {code: 1, label: 'Average'},
  GOOD: {code: 2, label: 'Good'},
  STRONG: {code: 3, label: 'Strong'}
};
