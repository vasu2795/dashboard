import React from "react";
import R from 'ramda';
import axios from 'axios';
import moment from 'moment';



const TRANSACTION_LIST_ALLOWED_COLUMNS = ["body","title","id","userId"];

class TransactionFilterModal extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      modalInstance: null,
      from: null,
      to: null,
      mobile: null
    };
  }

  open() {
    this.state.modalInstance.modal('show');
  }

  componentDidMount() {
    this.state.modalInstance = $('#filter-modal').modal();
    this.setState(this.state);
  }

  render() {
    let bindData = (key, event) => {
      this.state[key] = event.nativeEvent.target.value;
      this.setState(this.state);
    };

    let onClear = () => {
      this.state.modalInstance.modal('hide');
      this.props.onClear();
    };

    let onEnable = () => {
      this.state.modalInstance.modal('hide');
      this.props.onEnable(new Date(this.state.from).getTime(), new Date(this.state.to).getTime(), this.state.mobile);
    };

    return (
      <div className="ui small modal" id="filter-modal">
        <div className="header">
          Apply Filters
        </div>
        <div className="content">
          <div className="ui form">
            <div className="two fields">
              <div className="field">
                <label>From</label>
                <input type="date" onChange={(ev) => bindData('from', ev)} />
              </div>

              <div className="field">
                <label>To</label>
                <input type="date" onChange={(ev) => bindData('to', ev)} />
              </div>
            </div>
            <div className="field">
              <label>Mobile</label>
              <input type="number" onChange={(ev) => bindData('mobile', ev)} />
            </div>
            <button onClick={onClear} className="ui floated button">Clear Filter</button>
            <button onClick={onEnable} className="ui right floated green button">Apply Filter</button>
          </div>
        </div>
      </div>
    );
  }
}

export default class Transaction extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;

    let orderListFields = this.loadOrdersTablePrefs();

    if(!orderListFields) {
      orderListFields = R.pipe(
        R.reduce((aggObj, item) => {
          aggObj[item] = false;
          return aggObj;
        }, {}),
        R.reduce((aggObj, item) => {
          aggObj[item] = true;
          return aggObj;
        }, R.__, [])
      )(TRANSACTION_LIST_ALLOWED_COLUMNS);
    }

    this.state = {
      orders: {list: []},
      filters: {
        enabled: true,
        from: null,
        to: null,
        mobile: null
      },
      lastRefreshed: new Date(),
      isAjaxInProgress: false,
      ajaxError: false,
      merchantNotFound: false,
      orderListFields: orderListFields,
      currentStart: 1,
      perPageCount: 10,
      offset: 0,
      pageCount: 1,
      currentPage: null
    };
  }

  /*
    Load orders immediately
  */
  componentDidMount() {
    console.log("component did mount ");
    this.loadOrders();
    $('#table-settings').popup({
      hoverable: true
    });
  }

  componentWillReceiveProps(newProps) {
    console.log("component will receive props ");

    if(this.props.routeParams.merchant_id !== newProps.routeParams.merchant_id) {
      this.props = newProps;
      // this.loadOrders();
    }

    $('#table-settings').popup({
      hoverable: true
    });
  }


  loadOrdersTablePrefs() {
    console.log("loading table prefs ");
    return JSON.parse(localStorage.getItem('transactions_table_prefs'));
  }

  saveOrdersTablePrefs() {
    localStorage.setItem('transactions_table_prefs', JSON.stringify(this.state.orderListFields));
  }

  /*
    Load orders async.
    Later we can load over sockets.
  */
  loadOrders() {
    console.log("loadOrders ");
    let state = this.state;
    if(this.state.filters.enabled) {
      console.log("making call ?",this.state.filters);
      state.isAjaxInProgress = true;
      state.ajaxError = false;
      state.merchantNotFound = false;
      state.lastRefreshed = new Date();
      this.setState(state);
      let url = "https://jsonplaceholder.typicode.com/posts"
      axios.get(url).then((response) => {
        console.log("axios get success ",response.data);
        state.orders.list = response.data;
        state.isAjaxInProgress = false;
        state.filters.enabled = false;
        this.setState(state);
      }).catch((error) => {
        console.log("axios failed ")
        state.isAjaxInProgress = false;
        state.filters.enabled = false;
        this.setState(state);
      });
    }
  }

  openOrderDetail(orderId) {
    // let merchantId = this.props.routeParams.merchant_id;
    // dispatchNavigationEvent(`/merchants/${merchantId}/orders/${orderId}`);
  };

  toggleTableColumn(columnKey) {
    let state = this.state;
    if(state.orderListFields.hasOwnProperty(columnKey)) {
      state.orderListFields[columnKey] = !state.orderListFields[columnKey];
    }

    this.setState(state);

    this.saveOrdersTablePrefs();
  }

  /*
    Render the top orders.
  */
  render() {
    let fields = R.pipe(
      R.toPairs,
      R.filter((item) => {
        return item[1];
      }),
      R.map((item) => {
        return item[0];
      })
    )(this.state.orderListFields);

    let allFields = R.pipe(
      R.toPairs,
      R.map((item) => {
        return item[0]
      })
    )(this.state.orderListFields);
    console.log("allFields are ",allFields);

    let onEnable = (start, end, mobile) => {
      this.state.filters.enabled = true;
      this.state.filters.from = start;
      this.state.filters.to = end;
      this.state.filters.mobile = mobile;
      this.setState(this.state);

      this.loadOrders();
    };

    let onClear = () => {
      this.state.filters.enabled = false;
      this.state.filters.from = null;
      this.state.filters.to = null;
      this.setState(this.state);

      this.loadOrders();
    };

    return (
      <div className={"ui basic segment enterFrame" + (this.state.isAjaxInProgress ? ' loading' : '')}>
        <TransactionFilterModal onEnable={onEnable} onClear={onClear} ref="filters" />
        {do {
          let state = this.state;

          if(state.ajaxError) {
            <div className="ui error message">
              <div className="header">Oops!</div>
              <p>Looks like there was an error in processing your request.</p>
              <p>Click <a href="#">here</a> to try again.</p>
            </div>
          } else {
            [
              <div
                className={"ui mini icon labeled button" + (this.state.filters.enabled ? " green" : "")}
                onClick={(ev) => this.refs.filters.open()}
              >
                <i className="filter icon" />
                {do {
                  if(this.state.filters.enabled) {
                    "Filters Enabled"
                  } else {
                    "Set Filter"
                  }
                }}
              </div>,
              <div
                className="ui right floated mini blue icon labeled button"
                onClick={(ev) => ::this.loadOrders()}
              >
                <i className="refresh icon" />
                Refresh
              </div>,
              <table className="ui basic padded selectable segment table">
                <thead>
                  <tr>
                    {R.map((key) => {
                      return (
                        <th>{R.pipe(R.split('_'), R.join(' '))(key)}</th>
                      );
                    })(fields)}
                  </tr>
                </thead>
                <tbody>
                  {R.map((order) => {
                    return (
                      <tr>
                        {R.map((key) => {
                          return <td>{order[key]}</td>
                        }, fields)}
                      </tr>
                    );
                  }, this.state.orders.list || [])}
                </tbody>
              </table>,
              <div className="large left floated ui blue circular icon label" id="table-settings">
                <i className="ui settings icon" />
                Configure
              </div>,
              <div className="ui fluid popup top left transition hidden" id="table-settings-popup">
                <div className="ui two column divided grid">
                  <div className="column">
                    <h5 className="ui header">Fields</h5>
                    <div className="ui form">
                      {R.map((field) => {
                        console.log("field is ",field);
                          return (
                            <div className="field">
                              <div className={"ui toggle checkbox"}>
                                <input
                                  type="checkbox"
                                  checked={this.state.orderListFields[field]}
                                  onChange={(ev) => { ::this.toggleTableColumn(field) }} />
                                <label>{field}</label>
                              </div>
                            </div>
                          );
                        },(allFields || []))}
                    </div>
                  </div>
                </div>
              </div>
            ]
          }
        }}
      </div>
    );
  }
}
