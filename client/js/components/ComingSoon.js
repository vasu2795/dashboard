import React from 'react';

export default class ComingSoon extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div className="ui primary basic inverted segment" style={{
        background: "url('images/login-title-tile.png')",
        height: '80vh',
        borderRadius: '5px',
        marginTop: '10px'
      }}>
        <div classname="ui header" style={{
          color: '#FFFFFF',
          fontSize: '50px',
          textAlign: 'center',
          marginTop: '40vh'
        }}>Coming Soon</div>
      </div>
    )
  }
}
