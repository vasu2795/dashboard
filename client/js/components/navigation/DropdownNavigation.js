import React from 'react';
import R from 'ramda';

import dispatchNavigationEvent from '../../RoutingManagement';

export default class DropdownNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount () {
    $('#nav-dropdown').dropdown();
  }

  requestNavigation(item) {
    if(item.hasOwnProperty('expect')) {
      this.props.navHelper.requestNavigationInfo(item);
    } else {
      dispatchNavigationEvent(item.template.match(/(\S+)/ig).join(''));
    }
  }

  render() {
    let items = R.groupBy(R.prop('category'))(this.props.items);
    items = R.toPairs(items);

    return (
      <div className="ui top right pointing dropdown link item" id="nav-dropdown">
        <i className="bars icon" />
        <div className="vertical menu">
          {R.map((menu) => {
            return (
              <div className="item">
                <i className="left dropdown icon" />
                <span className="text">{menu[0]}</span>
                <div className="left vertical menu">
                  {R.map((item) => {
                    return (
                      <div onClick={(ev) => ::this.requestNavigation(item)} className="item">
                        <span className="text">{item.description}</span>
                      </div>
                    )
                  }, menu[1])}
                </div>
              </div>
            )
          }, items) }
        </div>
      </div>
    );
  }
}
