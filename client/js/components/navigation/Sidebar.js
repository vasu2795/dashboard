import React from 'react';
import R from 'ramda';
import axios from 'axios';

import store from '../../store/AppStore';

import dispatchNavigationEvent from '../../RoutingManagement';

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      selectedItem: {
        parent: 0,
        child: 0
      }
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      let currentUrl = store.getState()['Navigation'].lastVisited;
      R.pipe(
        R.addIndex(R.map)((route, index) => {
          R.addIndex(R.map)((childRoute, childIndex) => {
            let shouldNavigate = do {
              let regexMatched = R.length(R.match(childRoute.regex, currentUrl)) > 0;
              let pathsMatched = childRoute.template.match(/(\S+)/ig).join('');
              if(currentUrl.startsWith('/')) {
                currentUrl = currentUrl.substring(1);
              }
              regexMatched || (pathsMatched === currentUrl);
            }

            if(shouldNavigate) {
              this.state.selectedItem = {
                parent: index,
                child: childIndex
              };

              this.setState(this.state);
            }
          }, route[1]);
        })
      )(this.generateMenu())
    });
  }

  generateMenu() {
    let items = this.props.items;

    // Why were we ignoring the last element in the list
    // items = R.init(items);

    return R.pipe(
      R.reduce((agg, item) => {
        if(!agg.hasOwnProperty(item.category)) {
          agg[item.category] = [];
        }

        agg[item.category].push(item);

        return agg;
      }, {}),
      R.toPairs
    )(items);
  }

  componentWillReceiveProps(props) {
    this.props = props;
  }

  render() {
    let items = this.generateMenu();

    let selectedItem = this.state.selectedItem;

    return (
      <div className="ui secondary pointing left inverted primary fixed sidebar vertical menu visible" style={{top: '50px',paddingBottom: '100px'}}>
        {R.addIndex(R.map)((item, index) => {
          let children = R.addIndex(R.map)((child, childIndex) => {
            let requestNavigation = () => {
              if(child.hasOwnProperty('expect')) {
                this.props.navHelper.requestNavigationInfo(child).then(() => {
                  this.state.selectedItem = {
                    parent: index,
                    child: childIndex
                  };

                  this.setState(this.state);
                });
              } else {
                this.state.selectedItem = {
                  parent: index,
                  child: childIndex
                };

                this.setState(this.state);

                R.pipe(
                  R.split('/'),
                  R.map(R.trim),
                  R.reject(R.isEmpty),
                  R.join('/'),
                  dispatchNavigationEvent
                )(child.template)
              }
            };

            return (
              <a className={"item" + ((selectedItem.parent === index && selectedItem.child) === childIndex ? " active" : "")} onClick={requestNavigation}>
                {child.description}
              </a>
            )
          }, item[1]);

          return (
            <div className={"item" + (selectedItem.parent === index ? " active" : "")}>
              <div className="header">{item[0]}</div>
              <div className="menu">
                <div className="items">
                {children}
                </div>
              </div>
            </div>
          );
        }, items)}
      </div>
    );
  }
}
