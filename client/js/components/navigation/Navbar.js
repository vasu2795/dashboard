import React from 'react';
import axios from 'axios';
import R from 'ramda';

import { hashHistory } from 'react-router';

import store from '../../store/AppStore';
import { teardownApp } from '../../actions/AuthActions';
import dispatchNavigationEvent from '../../RoutingManagement';

/**
 * Notifications
 */
class Notification extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        notifications: [],
        hasUnreadUpdate: false
      };
    }

    // Set up realtime in here
    componentDidMount() {
      $('#notifications').dropdown();

      let chan = socket.channel('notification:realtime', {});
      chan.join()
        .receive('ignore', () => console.log('auth error'))
        .receive('ok', () => console.log('join ok'));

      chan.on('notification:realtime', payload => {
        let notifications = this.state.notifications;
        notifications.unshift(payload);

        if(notifications.length >= 10) {
          notifications.pop();
        }

        this.setState({
          notifications: notifications,
          hasUnreadUpdate: true
        });
      });
    }

    render() {
      return(
        <div id="notifications" className="ui top right pointing dropdown link item">
          <i className={"large mail outline icon" + (this.state.hasUnreadUpdate ? " orange" : "")}></i>
          <div className="menu">
            {R.map((notif) => {
              return (
                <div className="item">
                  <div className={"ui empty circular label " + (notif.type)}></div>
                  {notif.message}
                </div>
              );
            }, this.state.notifications)}
          </div>
        </div>
      );
    }
}

export default class Navbar extends React.Component {
    constructor(props) {
      super(props);
      this.props = props;
    }

    componentDidMount() {
      this.setState(this.state);
      $('#account-options').dropdown();
    }

    logout() {
      axios.get('/auth/logout').then((response) => {
        if(response.data.status === 'logged_out') {
          sessionStorage.clear();
          store.dispatch(teardownApp());
          hashHistory.push('/login')
        }
      }).catch((err) => {
        console.error(err);
      });
    }

    render() {
        return (
          <div id="topBar" className="ui top fixed menu" style={{'height': '50px', 'zIndex': '10000000'}}>
            <div className="item juspay-navbar-logo">
              <div className="image">
                <img  src="https://rapido.bike/img/logo-big.png" width="50" height="48"  />
              </div>
            </div>
            <div className="right menu">
              <div className="item">
                <div id="account-options" className="ui top right pointing dropdown">
                  <div className="text">{sessionStorage.getItem('username')}</div>
                  <i className="dropdown icon"></i>
                  <div className="menu">
                    <div className="item" onClick={::this.logout}>
                      Logout
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>);
    }
}
