import React from 'react';
import axios from 'axios';
import { hashHistory } from 'react-router';

import Footer from './Footer';

import store from '../store/AppStore';
import dispatchNavigationEvent from '../store/Navigation';
import { initApp } from '../actions/AuthActions';

export default class LoginForm extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        loginInProgress: false,
        showOtpField: false,
        resetProgress: false,
        loginError: false,
        forgot:false,
        loginform:true,
        resetStatus:false
      };
  }

  componentDidMount() {
    let loginStatus = localStorage.getItem('loginStatus');

    if(loginStatus === 'logged_in') {
      hashHistory.push('/app');
      store.dispatch(initApp());
    }
  }

  showOtpField = () => {
    this.state.showOtpField = true;
    this.setState(this.state);
  }

  doLogin = (ev) => {
		ev.nativeEvent.preventDefault();

    let username = this.refs.username.value;
    let password = this.refs.password.value;


    this.setState({
      loginInProgress: true,
      loginError: false
    });

    axios.post('/auth/login', {
      username: username,
      password: password
    }).then((response) => {
      console.log("Response is ",response);
      response = response.data;

      if(response.token) {
        this.setState({
          loginInProgress: false,
          loginError: false
        });

        // Persist login status into sessionStorage
        sessionStorage.setItem('loginStatus', 'logged_in');
        sessionStorage.setItem('loginToken', "Bearer " + response.data.token);

        // sessionStorage.setItem('roles', JSON.stringify(["admin,user"]));
        store.dispatch(initApp());
        hashHistory.push('/app');
      } else {
        this.state.loginInProgress = false;
        this.state.loginError = "Servers unable to authenticate ! Please try again"
              this.setState(this.state);

      }
        // store.dispatch({type: 'ADD_MERCHANTS', merchants: response.merchants});
      // }
    }).catch((error) => {
      this.state.loginInProgress = false;

      if(error.response) {
        let cause = error.data.message;
        this.state.loginError = cause;
      }

      this.setState(this.state);
    });
  }

  doForgot = (ev) => {
  //   ev.nativeEvent.preventDefault();
  //   let email = this.refs.email.value;
  //
  //   this.setState({
  //     resetProgress: true,
  //     resetStatus: false
  //   });
  //
  //   axios.post('/auth/forgot',{
  //     email: email
  //   }).then((response) => {
  //     response = response.data;
  //     if(response.status === "success"){
  //       this.setState({
  //         resetProgress: false,
  //         resetStatus: 'success'
  //       });
  //     }
  //   }).catch((error) => {
  //     this.state.resetProgress = false;
  //     this.state.resetStatus = 'failed';
  //     this.setState(this.state);
  //   });
  }

  forgotPassword = () => {
    // this.state.forgot = true;
    // this.state.loginform = false;
    // this.state.resetStatus = false;
    // this.state.loginError = false;
    // this.setState(this.state);
  }

  showForm = () => {
    this.state.forgot = false;
    this.state.loginform = true;
    this.state.resetStatus= false;
    this.state.loginError= false;
    this.setState(this.state);
  }

  render() {

    return (
      <div>
        <div className="ui top fixed menu" style={{'height': '60px', 'zIndex': '10000000'}}>
          <div className="item juspay-navbar-logo">
            <div className="image">
              <img className="logo" width="180" height="48"  />
            </div>
          </div>
        </div>
        <div className="ui primary inverted advert segment" style={{
          background: "url('images/login-title-tile.png')",
          top: '60px',
          margin: '0px',
          borderRadius: '0px',
          position: 'relative'
        }}>
          <h1 style={{
            fontSize: '40px',
            fontWeight: 'normal',
            color: 'white',
            padding: '30px',
            textAlign: 'center'
          }}>Rapido Dashboard Login</h1>
        </div>
        <div className="ui basic segment">
          <div className="three column ui grid">
            <div className="content column">&nbsp;</div>
            <div className="ui segment content column" style={{'top': '-50%', 'transform': 'translateY(30%)'}}>
              <div style={{
                fontWeight: 'normal',
                fontSize: '25px',
                textAlign: 'center'
              }} className="ui header">
                Login
              </div>
              <div className="ui hidden divider" />
              <div className="description">
                {do {
                   if(this.state.loginError === 'invalid_creds') {
                     <div className="ui error message">
                       Oops! We could not validate your username and password.
                       Please try again.
                     </div>
                   } else if(this.state.loginError === 'invalid_otp') {
                     <div className="ui error message">
                       Please enter a valid OTP to login
                     </div>
                   }
                }}

                {do {
                  if(this.state.resetStatus == 'success') {
                    <div className="ui success message">
                      Please check your Mail for Password Reset Link
                    </div>
                  }
                }}

                <div>
                {do {
                   if(this.state.loginform) {
                     <form className="ui form" onSubmit={this.doLogin} id="login_form">
                       <div className="ui fluid input field">
                         <input type="text" ref="username" placeholder="Username" />
                       </div>
                       <div className="ui fluid input field">
                         <input type="password" ref="password" placeholder="Password" />
                       </div>
                    <div className="two ui buttons">
                         <a className="ui basic button" onClick={this.forgotPassword}>Forgot Pasword</a>
                         <button
                             style={{
                               borderRadius: '0'
                             }}
                             className={"ui primary button" + (this.state.loginInProgress ? ' disabled loading' : '')}
                         >
                           Login
                         </button>
                       </div>
                     </form>
                   }
                }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


// <div>
// {do {
//    if(this.state.forgot) {
//      let nodes = [
//        <form className="ui form" id="forgot_form" onSubmit={this.doForgot}>
//          <div className="ui fluid input field">
//            <input type="text" ref="email" placeholder="Email" required/>
//          </div>
//          <div>
//            <a className="ui basic button"  onClick={this.showForm} >Back</a>
//            <button
//
//                className={"ui primary button" + (this.state.resetProgress ? ' disabled loading' : '')}
//            >Reset</button>
//          </div>
//        </form>
//      ];
//
//      if(this.state.resetStatus == 'failed') {
//        nodes.unshift(
//          <div className="ui error message">
//            <div className="header">Failed to send link</div>
//            Failed to send reset password link. Please try again.
//          </div>
//        );
//      }
//
//      nodes
//    }
// }}
// </div>
