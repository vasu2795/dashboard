import React from 'react';
import R from 'ramda';

import Navbar from './navigation/Navbar';
import Sidebar from './navigation/Sidebar';

import Footer from './Footer';

import dispatchNavigationEvent from '../RoutingManagement';

export default class MainApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.prepareNavigationItems()
    };
  }

  prepareNavigationItems() {
    let items = [];
    let adminItems = [{
          category: 'Payments',
          regex: /^payments\/transactions\$/i,
          template: `payments / transactions`,
          description: 'Trasnsactions'
        },{
          category: 'Payments',
          regex: /^payments\/dummy\$/i,
          template: `payments / dummy`,
          description: 'Dummy'
        }]

    items.push(...adminItems);

    items = R.uniqBy(R.prop('template'))(items);

    return items;
  }

  loadHome() {
    let location = R.pipe(R.split('/'), R.map(R.trim), R.reject(R.isEmpty), R.join('/'))(this.state.items[0].template);
    dispatchNavigationEvent(location);
  }

  componentDidMount() {
    console.log("mounting main app ");
    this.loadHome();
  }

  render() {
    return (
      <div id="container">
        <Navbar navHelper={this.refs.navHelper} items={this.state.items} />
        <Sidebar navHelper={this.refs.navHelper} items={this.state.items} />
        <div style={{
          marginBottom: '50px'
        }}>
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}
