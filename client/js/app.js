
import ReactDOM from 'react-dom';
import React from 'react';
import axios from 'axios';

import store from './store/AppStore';
import { initApp } from './actions/AuthActions';
import ComingSoon from './components/ComingSoon';
import LoginForm from './components/LoginForm';
import MainApp from './components/MainApp';
import Transaction from "./components/payments/Transaction";
import { Router, Route, IndexRoute, hashHistory } from 'react-router';


// axios.defaults.headers.common['Version'] = '2016-07-19';

let currentUrl = window.location.href;

if(!currentUrl.includes("/reset")){
  let loginStatus = sessionStorage.getItem('loginStatus');
  if(loginStatus && loginStatus === 'logged_in') {
    hashHistory.push('/app');
  } else {
    hashHistory.push('/app');
  }
}

hashHistory.listen((args) => {
  let simplePath = args.pathname.split('/').join('');
  let currentSimplePath = store.getState()['Navigation'].lastVisited.split('/').join('');

  if(simplePath != currentSimplePath) {
		/*
			Do not call dispatchNavigationEvent here.
			Dispatch navigation event will update the router history ->
			and the snake eats itself.
		 */
    store.dispatch({
      type: 'NAVIGATION_ACTION',
      to: args.pathname
    });
  }
});

ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/login" component={LoginForm} />
    <Route path="/app" component={MainApp}>
      <Route path="/payments/transactions" component={Transaction} />
      <Route path="/payments/dummy" component={ComingSoon} />
    </Route>
	</Router>, document.getElementById('application'));
